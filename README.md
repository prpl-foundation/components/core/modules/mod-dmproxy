# Module Data Model Proxy

[[_TOC_]]

## Introduction

**TODO**

## Building, Installing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

Please read [Getting Started](https://gitlab.com/prpl-foundation/components/ambiorix/tutorials/getting-started/-/blob/main/README.md) to create a local workspace and to set-up a local development environment.

More information can be found at:

- [Debug & development environment](https://gitlab.com/prpl-foundation/components/ambiorix/dockers/amx-sah-dbg/-/blob/main/README.md) to set-up a local development environment.

### Building

#### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc) - Generic C api for common data containers
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp) - Common patterns implementations
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo) - The ODL compiler library
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd) - Data model C API


### Build mod-dm-extension

1. Clone the git repository

    To be able to build it, you need the source code. So clone this git-repo to your workspace

    ```bash
    mkdir -p ~/development/amx
    cd ~/development/amx
    git clone git@gitlab.com:prpl-foundation/components/core/modules/mod-dmproxy.git
    ``` 

1. Install dependencies

    Although the container will contain all tools needed for building, it does not contain the libraries needed for building. Install all libraries needed. When you created the local workspace and followed the [Getting Started](https://gitlab.com/prpl-foundation/components/ambiorix/tutorials/getting-started/-/blob/main/README.md) guide, everything should be installed.

    As an alternative it is possible to installed pre-compiled packages in the development container using:

    ```bash
    sudo apt update
    sudo apt install libamxc libamxd libamxp libamxb libamxo
    ```

1. Build it

    ```bash
    cd ~/development/amx/mod_dmproxy
    make
    ```

1. Install it

    When the build is successful, install the binary at the correct location.

    ```bash
    sudo -E make install
    ```

## Using module data model proxy

### Import the module

In your main odl file `import` the shared object of this module.

In the config section define the object path that will be used as root of the proxy object.

Optionally other mappings can be defined.

---
> **NOTE**<br>
> When using ubus as bus system, make sure that your data model is registered to ubus on the start event.
> This module will overide some of the default methods of the proxy object and will add some.
---

Make sure the entry-point is added, so the module can initialize itself.

Example:

```odl
%config {
    name = "MyDM";

    import-dbg = true;

    // main files
    definition_file = "${name}_definition.odl";

    proxy-object = {
        "Device." = "",
        "Device.IP." = "IP.",
        "Device.IP.Diagnostics." = "IPDiagnostics."
    };

    ubus = {
        register-on-start-event = true
    };
}

import "mod-dmproxy.so" as "proxy";

include "${definition_file}";

%define {
    entry-point proxy.dm_proxy_main;
}
```
