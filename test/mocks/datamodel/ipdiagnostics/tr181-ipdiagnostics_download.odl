/**
 * IPDiagnostics.DownloadDiagnostics from TR-181 datamodel 2.15.1
 *
 * @version 1.0
 */
%define {
    select IPDiagnostics {
        /**
         * Comma-separated list of strings. Supported DownloadDiagnostics transport protocols for a CPE device. Each list item is an enumeration of:
         * - HTTP
         * - FTP (OPTIONAL)
         *
         * @version 1.0
         */
        %read-only csv_string DownloadTransports;

        /**
         * Indicates the maximum number of connections that are supported by Download Diagnostics.
         *
         * @version 1.0
         */
        %read-only uint32 DownloadDiagnosticMaxConnections = 1 {
            on action validate call check_minimum 1;
        }

        /**
         * The maximum number of instances in DownloadDiagnostics().IncrementalResult that the implementation will return.
         *
         * @version 1.0
         */
        %read-only uint32 DownloadDiagnosticsMaxIncrementalResult;

        /*
        * An object that contains the DownloadDiagnostics configuration.
        *
        * @version 1.0
        */
        %persistent object '${prefix_}DownloadConfig' {
            /*
             * The value MUST be the Path Name of a table row. The IP-layer interface over which the test is to be performed.
             * Example: Device.IP.Interface.1
             * 
             * If an empty string is specified, the CPE MUST use the interface as directed by its routing policy
             * (Forwarding table entries) to determine the appropriate interface.
             *
             * @version 1.0
             */
            %persistent string Interface {
                on action validate call check_maximum_length 256;
            }

            /*
             * The [URL] for the CPE to perform the download on.
             * This parameter MUST be in the form of a valid HTTP [RFC2616] or FTP [RFC959] URL.
             * 
             * - When using FTP transport, FTP binary transfer MUST be used.
             * - When using HTTP transport, persistent connections MUST be used and pipelining MUST NOT be used.
             * - When using HTTP transport the HTTP Authentication MUST NOT be used.
             *
             * Note: For time based tests (TimeBasedTestDuration > 0) the Controller MAY add a hint to duration of the test
             * to the URL. See [Section 4.3/TR-143] for more details.
             *
             * @version 1.0
             */
            %persistent string DownloadURL {
                on action validate call check_maximum_length 2048;
            }

            /*
             * The DiffServ code point for marking packets transmitted in the test.
             *
             * @version 1.0
             */
            %persistent uint32 DSCP = 0 {
                on action validate call check_maximum 63;
            }

            /*
             * Ethernet priority code for marking packets transmitted in the test (if applicable).
             *
             * @version 1.0
             */
            %persistent uint32 EthernetPriority = 0 {
                on action validate call check_maximum 7;
            }

            /*
             * Controls time based testing [Section 4.3/TR-143]. When TimeBasedTestDuration > 0, TimeBasedTestDuration is the
             * duration in seconds of a time based test. If TimeBasedTestDuration is 0, the test is not based on time, but on
             * the size of the file to be downloaded.
             *
             * @version 1.0
             */
            %persistent uint32 TimeBasedTestDuration = 0 {
                on action validate call check_maximum 999;
            }

            /*
             * The measurement interval duration in seconds for objects in IncrementalResult for a time based FTP/HTTP download test
             * (when TimeBasedTestDuration > 0). 
             *
             * For example if TimeBasedTestDuration is 90 seconds and TimeBasedTestMeasurementInterval is 10 seconds,
             * there will be 9 results in IncrementalResult, each with a 10 seconds duration.
             *
             * @version 1.0
             */
            %persistent uint32 TimeBasedTestMeasurementInterval = 0 {
                on action validate call check_maximum 999;
            }

            /*
             * This TimeBasedTestMeasurementOffset works in conjunction with TimeBasedTestMeasurementInterval to allow the interval
             * measurement to start a number of seconds after BOMTime. The test measurement interval in IncrementalResult starts at
             * time BOMTime + TimeBasedTestMeasurementOffset to allow for slow start window removal of file transfers. 
             *
             * This TimeBasedTestMeasurementOffset is in seconds.
             *
             * @version 1.0
             */
            %persistent uint32 TimeBasedTestMeasurementOffset = 0 {
                on action validate call check_maximum 255;
            }

            /*
             * Indicates the IP protocol version to be used. Enumeration of:
             * - Any (Use either IPv4 or IPv6 depending on the system preference)
             * - IPv4 (Use IPv4 for the requests)
             * - IPv6 (Use IPv6 for the requests)
             *
             * @version 1.0
             */
            %persistent string ProtocolVersion = "Any" {
                on action validate call check_is_in "IPDiagnostics.ProtocolVersionsAllowed";
            }

            /*
             * The number of connections to be used in the test. The default value SHOULD be 1.
             * NumberOfConnections MUST NOT be set to a value greater than DownloadDiagnosticMaxConnections.
             *
             * @version 1.0
             */
            %persistent uint32 NumberOfConnections = 1 {
                on action validate call check_minimum 1;
                on action validate call check_maximum "IPDiagnostics.DownloadDiagnosticMaxConnections";
            }

            /*
             * The results must be returned in the PerConnectionResult table for every connection when set to true.
             *
             * @version 1.0
             */
            %persistent bool EnablePerConnectionResults = false;

        }

        /*
        * An object that contains the DownloadDiagnostics result.
        *
        * @version 1.0
        */
        object '${prefix_}DownloadResult'[] {
            counted with '${prefix_}NumberOfDownloadResult';

            /*
             * Indicates the availability of diagnostics data. Enumeration of:
             * 
             * - Complete
             * - Error_CannotResolveHostName
             * - Error_NoRouteToHost
             * - Error_InitConnectionFailed
             * - Error_NoResponse
             * - Error_TransferFailed
             * - Error_PasswordRequestFailed
             * - Error_LoginFailed
             * - Error_NoTransferMode
             * - Error_NoPASV
             * - Error_IncorrectSize
             * - Error_Timeout
             * - Error_Internal
             * - Error_Other
             *
             * If the value of this parameter is anything other than Complete, the values of the other results parameters
             * for this test are indeterminate.
             *
             * @version 1.0
             */
            %read-only string Status = "Error_Internal"{
                on action validate call check_enum ["Complete",
                                                    "Error_CannotResolveHostName",
                                                    "Error_NoRouteToHost",
                                                    "Error_InitConnectionFailed",
                                                    "Error_NoResponse",
                                                    "Error_TransferFailed",
                                                    "Error_PasswordRequestFailed",
                                                    "Error_LoginFailed",
                                                    "Error_NoTransferMode",
                                                    "Error_NoPASV",
                                                    "Error_IncorrectSize",
                                                    "Error_Timeout",
                                                    "Error_Internal",
                                                    "Error_Other"];
            }

            /*
             * [IPAddress] Indicates which IP address was used to send the request.
             *
             * @version 1.0
             */
            %read-only string IPAddressUsed {
                on action validate call check_maximum_length 45;
            }

            /*
             * Request time in UTC, which MUST be specified to microsecond precision.
             *
             * For example: 2008-04-09T15:01:05.123456Z
             * - For HTTP this is the time at which the client sends the GET command.
             * - For FTP this is the time at which the client sends the RTRV command.
             *
             * If multiple connections are used, then ROMTime is set to the earliest ROMTime across all connections.
             *
             * @version 1.0
             */
            %read-only datetime ROMTime;

            /*
             * Begin of transmission time in UTC, which MUST be specified to microsecond precision
             *
             * For example: 2008-04-09T15:01:05.123456Z
             * - For HTTP this is the time at which the first data packet is received.
             * - For FTP this is the time at which the client receives the first data packet on the data connection.
             *
             * If multiple connections are used, then BOMTime is set to the earliest BOMTime across all connections.
             *
             * @version 1.0
             */
            %read-only datetime BOMTime;

            /*
             * End of transmission in UTC, which MUST be specified to microsecond precision.
             *
             * For example: 2008-04-09T15:01:05.123456Z
             * - For HTTP this is the time at which the last data packet is received.
             * - For FTP this is the time at which the client receives the last packet on the data connection.
             *
             * If multiple connections are used, then EOMTime is set to the latest EOMTime across all connections.
             *
             * @version 1.0
             */
            %read-only datetime EOMTime;

            /*
             * The number of bytes received during the FTP/HTTP transaction including FTP/HTTP headers,
             * between BOMTime and EOMTime across all connections.
             *
             * @version 1.0
             */
            %read-only uint32 TestBytesReceived;

            /*
             * The total number of bytes (at the IP layer) received on the Interface between BOMTime and EOMTime.
             * This MAY be calculated by sampling Stats.BytesReceived on the Interface object at BOMTime and at EOMTime and subtracting. 
             * If Interface is an empty string, this parameter cannot be determined and SHOULD be 0.
             *
             * @version 1.0
             */
            %read-only uint32 TotalBytesReceived;

            /*
             * The total number of bytes (at the IP layer) sent on the Interface between BOMTime and EOMTime.
             * This MAY be calculated by sampling Stats.BytesSent on the Interface object at BOMTime and at EOMTime and subtracting.
             * If Interface is an empty string, this parameter cannot be determined and SHOULD be 0.
             *
             * @version 1.0
             */
            %read-only uint32 TotalBytesSent;

            /*
             * The number of bytes of the test file received between the latest PerConnectionResult.{i}.BOMTime
             * and the earliest PerConnectionResult.{i}.EOMTime across all connections.
             *
             * @version 1.0
             */
            %read-only uint32 TestBytesReceivedUnderFullLoading;

            /*
             * The total number of bytes (at the IP layer) received in between the latest PerConnectionResult.{i}.BOMTime
             * and the earliest PerConnectionResult.{i}.EOMTime. This MAY be calculated by sampling Stats.BytesReceived on
             * the Interface object at the latest PerConnectionResult.{i}.BOMTime and at the earliest PerConnectionResult.{i}.EOMTime and subtracting.
             *
             * @version 1.0
             */
            %read-only uint32 TotalBytesReceivedUnderFullLoading;

            /*
             * The total number of bytes (at the IP layer) sent between the latest PerConnectionResult.{i}.BOMTime and the
             * earliest PerConnectionResult.{i}.EOMTime. This MAY be calculated by sampling Stats.BytesSent on the Interface
             * object at the latest PerConnectionResult.{i}.BOMTime and at the earliest PerConnectionResult.{i}.EOMTime and subtracting.
             *
             * @version 1.0
             */
            %read-only uint32 TotalBytesSentUnderFullLoading;

            /*
             * The period of time in microseconds between the latest PerConnectionResult.{i}.BOMTime and the earliest PerConnectionResult.{i}.EOMTime of the test.
             *
             * @version 1.0
             */
            %read-only uint32 PeriodOfFullLoading;

            /*
             * Request time in UTC, which MUST be specified to microsecond precision.
             *
             * For example: 2008-04-09T15:01:05.123456Z
             * - For HTTP this is the time at which the TCP socket open (SYN) was sent for the HTTP connection.
             * - For FTP this is the time at which the TCP socket open (SYN) was sent for the data connection.
             *
             * Note: Interval of 1 microsecond SHOULD be supported.
             *
             * If multiple connections are used, then TCPOpenRequestTime is set to the latest TCPOpenRequestTime across all connections.
             *
             * @version 1.0
             */
            %read-only datetime TCPOpenRequestTime;

            /*
             * Response time in UTC, which MUST be specified to microsecond precision.
             *
             * For example: 2008-04-09T15:01:05.123456Z
             * - For HTTP this is the time at which the TCP ACK to the socket opening the HTTP connection was received.
             * - For FTP this is the time at which the TCP ACK to the socket opening the data connection was received.
             *
             * Note: Interval of 1 microsecond SHOULD be supported.
             *
             * If multiple connections are used, then TCPOpenResponseTime is set to the latest TCPOpenResponseTime across all connections.
             *
             * @version 1.0
             */
            %read-only datetime TCPOpenResponseTime;

            /*
            * Results for individual connections. This table is only populated when EnablePerConnectionResults is true.
            * A new object is created for each connection specified in NumberOfConnections. Instance numbers MUST start
            * at 1 and sequentially increment as new instances are created.
            *
            * This table's Instance Numbers MUST be 1, 2, 3... (assigned sequentially without gaps).
            *
            * @version 1.0
            */
            object PerConnectionResult[] {
                /*
                * Request time in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                * - For HTTP this is the time at which the client sends the GET command.
                * - For FTP this is the time at which the client sends the RTRV command.
                *
                * @version 1.0
                */
                %read-only datetime ROMTime;

                /*
                * Begin of transmission time in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                * - For HTTP this is the time at which the first data packet is received.
                * - For FTP this is the time at which the client receives the first data packet on the data connection.
                *
                * @version 1.0
                */
                %read-only datetime BOMTime;

                /*
                * End of transmission in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                * - For HTTP this is the time at which the last data packet is received.
                * - For FTP this is the time at which the client receives the last packet on the data connection.
                *
                * @version 1.0
                */
                %read-only datetime EOMTime;

                /*
                * The number of bytes of the test file received during the FTP/HTTP transaction including FTP/HTTP headers, between BOMTime and EOMTime.
                *
                * @version 1.0
                */
                %read-only uint32 TestBytesReceived;

                /*
                * The total number of bytes (at the IP layer) received on the Interface between BOMTime and EOMTime.
                * This MAY be calculated by sampling Stats.BytesReceived on the Interface object at BOMTime and at EOMTime and subtracting.
                *
                * @version 1.0
                */
                %read-only uint32 TotalBytesReceived;

                /*
                * The total number of bytes (at the IP layer) sent on the Interface between BOMTime and EOMTime. This MAY be calculated by sampling
                * Stats.BytesSent on the Interface object at BOMTime and at EOMTime and subtracting.
                *
                * @version 1.0
                */
                %read-only uint32 TotalBytesSent;

                /*
                * Request time in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                * - For HTTP this is the time at which the TCP socket open (SYN) was sent for the HTTP connection.
                * - For FTP this is the time at which the TCP socket open (SYN) was sent for the data connection.
                *
                * @version 1.0
                */
                %read-only datetime TCPOpenRequestTime;

                /*
                * Response time in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                * - For HTTP this is the time at which the TCP ACK to the socket opening the HTTP connection was received.
                * - For FTP this is the time at which the TCP ACK to the socket opening the data connection was received.
                *
                * @version 1.0
                */
                %read-only datetime TCPOpenResponseTime;
            }

            /*
            * Results for time segmented tests (tests where TimeBasedTestDuration > 0 and TimeBasedTestMeasurementInterval > 0).
            * This data is totaled across all connections in the test. A new object is created every TimeBasedTestMeasurementInterval
            * after that interval has completed. Instance numbers MUST start at 1 and sequentially increment as new instances are created.
            *
            * This table's Instance Numbers MUST be 1, 2, 3... (assigned sequentially without gaps).
            *
            * @version 1.0
            */
            object IncrementalResult[] {
                /*
                * Change in the value of TestBytesReceivedUnderFullLoading between StartTime and EndTime.
                *
                * @version 1.0
                */
                %read-only uint32 TestBytesReceived;

                /*
                * The total number of bytes (at the IP layer) received on the Interface between StartTime and EndTime.
                * This MAY be calculated by sampling Stats.BytesReceived on the Interface object at StartTime and at EndTime and subtracting.
                * If Interface is an empty string, this parameter cannot be determined and SHOULD be 0.
                *
                * @version 1.0
                */
                %read-only uint32 TotalBytesReceived;

                /*
                * The total number of bytes (at the IP layer) sent on the Interface between StartTime and EndTime.
                * This MAY be calculated by sampling Stats.BytesSent on the Interface object at StartTime and at EndTime and subtracting.
                * If Interface is an empty string, this parameter cannot be determined and SHOULD be 0.
                *
                * @version 1.0
                */
                %read-only uint32 TotalBytesSent;

                /*
                * The start time of this interval which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                *
                * @version 1.0
                */
                %read-only datetime StartTime;

                /*
                * The end time of this interval which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                *
                * @version 1.0
                */
                %read-only datetime EndTime;
            }
        }
    }
}
