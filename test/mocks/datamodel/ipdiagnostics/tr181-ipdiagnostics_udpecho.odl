/**
 * IPDiagnostics.UDPEchoDiagnostics from TR-181 datamodel 2.15.1
 *
 * @version 1.0
 */
%define {
    select IPDiagnostics {
        /**
         * The maximum number of rows in UDPEchoDiagnostics().IndividualPacketResult that the CPE will store.
         *
         * @version 1.0
         */
        %read-only uint32 UDPEchoDiagnosticsMaxResults;
        
        /*
        * An object that contains the UDPEchoDiagnostics configuration.
        *
        * @version 1.0
        */
        %persistent object '${prefix_}UDPEchoDiagnostics' {
            /*
             * The value MUST be the Path Name of the IP-layer interface over which the test is to be performed.
             * If the referenced object is deleted, the parameter value MUST be set to an empty string.
             * Example: Device.IP.Interface.1
             * 
             * If an empty string is specified, the CPE MUST use the interface as directed by its bridging or routing policy
             * (Forwarding table entries) to determine the appropriate interface.
             *
             * @version 1.0
             */
            %persistent string Interface {
                on action validate call check_maximum_length 256;
            }

            /*
             * Host name or address of the host to perform tests to.
             *
             * @version 1.0
             */
            %persistent string Host {
                on action validate call check_maximum_length 256;
            }

            /*
             * Port on the host to perform tests to.
             *
             * @version 1.0
             */
            %persistent uint32 Port = 1 {
                on action validate call check_maximum 65535;
                on action validate call check_minimum 1;
            }

            /*
             * Number of repetitions of the test to perform before reporting the results.
             *
             * @version 1.0
             */
            %persistent uint32 NumberOfRepetitions = 1 {
                on action validate call check_minimum 1;
            }

            /*
             * Timeout in milliseconds for the test. That is, the amount of time to wait for
             * the return of a packet that was sent to the Host.
             *
             * @version 1.0
             */
            %persistent uint32 Timeout = 1 {
                on action validate call check_minimum 1;
            }

            /*
             * Size of the data block in bytes to be sent for each trace route.
             *
             * @version 1.0
             */
            %persistent uint32 DataBlockSize = 24 {
                on action validate call check_maximum 65535;
                on action validate call check_minimum 1;
            }

            /*
             * DiffServ codepoint to be used for the test packets.
             *
             * @version 1.0
             */
            %persistent uint32 DSCP = 0 {
                on action validate call check_maximum 63;
            }

            /*
             * The time in milliseconds between the NumberOfRepetitions of packets sent during a given test.
             *
             * @version 1.0
             */
            %persistent uint32 InterTransmissionTime = 1000 {
                on action validate call check_maximum 65535;
                on action validate call check_minimum 1;
            }

            /*
             * Indicates the IP protocol to be used. Enumeration of:
             * 
             * - Any (Use either IPv4 or IPv6 depending on the system preference)
             * - IPv4 (Use IPv4 for the Ping requests)
             * - IPv6 (Use IPv6 for the Ping requests)
             *
             * @version 1.0
             */
            %persistent string ProtocolVersion = "Any" {
                on action validate call check_is_in "IPDiagnostics.ProtocolVersionsAllowed";
            }

            /*
             * The results must be returned in the IndividualPacketResult table for every repetition of
             * the test when set to true.
             *
             * @version 1.0
             */
            %persistent bool EnableIndividualPacketResults = false;
        }

        /*
        * An object that contains the UDPEchoDiagnostics result.
        *
        * @version 1.0
        */
        object '${prefix_}UDPEchoDiagnosticsResult'[] {
            counted with '${prefix_}NumberOfUDPEchoDiagnosticsResult';

            /*
             * Indicates the availability of diagnostics data. Enumeration of:
             * 
             * - Complete
             * - Error (OPTIONAL)
             * - Error_CannotResolveHostName
             * - Error_Internal
             * - Error_Other
             *
             * If the value of this parameter is anything other than Complete, the values of the other results parameters
             * for this test are indeterminate.
             *
             * @version 1.0
             */
            %read-only string Status = "Error_Internal" {
                on action validate call check_enum ["Complete",
                                                    "Error",
                                                    "Error_CannotResolveHostName",
                                                    "Error_Internal",
                                                    "Error_Other"];
            }

            /*
             * Indicates which IP address was used to send the request.
             *
             * @version 1.0
             */
            %read-only string IPAddressUsed {
                on action validate call check_maximum_length 45;
            }

            /*
             * Result parameter indicating the number of successful pings
             * (those in which a successful response was received prior to the timeout) in the most recent ping test.
             *
             * @version 1.0
             */
            %read-only uint32 SuccessCount;

            /*
             * Result parameter indicating the number of failed pings in the most recent ping test.
             *
             * @version 1.0
             */
            %read-only uint32 FailureCount;

            /*
             * Result parameter indicating the average response time in milliseconds over all repetitions with
             * successful responses of the most recent ping test.
             *
             * @version 1.0
             */
            %read-only uint32 AverageResponseTime;

            /*
             * Result parameter indicating the minimum response time in milliseconds over all repetitions with
             * successful responses of the most recent ping test.
             *
             * @version 1.0
             */
            %read-only uint32 MinimumResponseTime;

            /*
             * Result parameter indicating the maximum response time in milliseconds over all repetitions with
             * successful responses of the most recent ping test.
             *
             * @version 1.0
             */
            %read-only uint32 MaximumResponseTime;

            /*
            * This object provides the results from individual UDPEchoPlus test packets collected during a test if
            * EnableIndividualPacketResults is set to true. It should contain NumberOfRepetitions objects. Instance
            * numbers MUST start at 1 and sequentially increment as new instances are created. The instance number
            * should match the TestIterationNumber field of the request and response packet.
            *
            * This table's Instance Numbers MUST be 1, 2, 3... (assigned sequentially without gaps).
            *
            * @version 1.0
            */
            object IndividualPacketResult[] {
                /*
                * Indicates that the response to this UDP Echo Plus packet sent was received by the client. When this
                * value is true, then all the remaining parameters in this instance are valid. Otherwise only the values
                * originally set by the CPE client (e.g. PacketSendTime and TestGenSN) MAY be set to valid values.
                *
                * @version 1.0
                */
                %read-only bool PacketSuccess;

                /*
                * Time the client sent this UDP Echo Plus packet in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                *
                * @version 1.0
                */
                %read-only datetime PacketSendTime;

                /*
                * Time the client receives the response packet in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                *
                * If this response is never received, PacketReceiveTime SHOULD be set to the Unknown Time value as specified
                * in [Section 3.2.2/TR-106].
                *
                * @version 1.0
                */
                %read-only datetime PacketReceiveTime;

                /*
                * The TestGenSN field in the UDPEcho Plus packet [Section A.1.4/TR-143] sent by the CPE client.	
                *
                * @version 1.0
                */
                %read-only uint32 TestGenSN;

                /*
                * The TestRespSN field in the response packet [Section A.1.4/TR-143] from the UDP Echo Plus server (i.e. Host)
                * for this Echo Plus packet sent by the CPE client.
                *
                * @version 1.0
                */
                %read-only uint32 TestRespSN;

                /*
                * The TestRespRcvTimeStamp field in the response packet [Section A.1.4/TR-143] from the UDP Echo Plus server
                * (i.e. Host) to record the reception time of this UDP Echo Plus packet sent from the CPE client
                *
                * @version 1.0
                */
                %read-only uint32 TestRespRcvTimeStamp;

                /*
                * The TestRespReplyTimeStamp field in the response packet [Section A.1.4/TR-143] from the UDP Echo Plus server
                * (i.e. Host) to record the server reply time of this UDP Echo Plus packet sent from the CPE client.
                *
                * @version 1.0
                */
                %read-only uint32 TestRespReplyTimeStamp;

                /*
                * The count value that was set by the UDP Echo Plus server (i.e. Host) to record the number of dropped echo
                * response packets by the server. This count is incremented if a valid echo request packet is received at a
                * UDP EchoPlus server but for some reason cannot be responded to (e.g. due to local buffer overflow,
                * CPU utilization, etc...).
                *
                * @version 1.0
                */
                %read-only uint32 TestRespReplyFailureCount;

            }

        }
    }
}
