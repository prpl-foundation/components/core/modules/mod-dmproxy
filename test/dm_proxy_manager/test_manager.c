/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <sys/signalfd.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "dm_proxy.h"

#include <amxb/amxb_register.h>

#include "../mocks/test_common.h"

#include "test_manager.h"

static amxb_bus_ctx_t* bus_ctx = NULL;

int test_dm_proxy_setup(UNUSED void** state) {
    system("ubusd &");
    system("amxrt ../mocks/datamodel/remote_dm.odl &");
    system("amxrt ../mocks/test_valid.odl &");

    assert_int_equal(amxb_be_load("/usr/bin/mods/amxb/mod-amxb-ubus.so"), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "ubus:"), 0);
    amxb_set_access(bus_ctx, amxd_dm_access_public);

    sleep(1);

    assert_int_equal(amxb_wait_for_object("IPDiagnostics."), 0);
    assert_int_equal(amxb_wait_for_object("Device."), 0);
    assert_int_equal(amxb_wait_for_object("Device.IP."), 0);

    return 0;
}

int test_dm_proxy_teardown(UNUSED void** state) {
    system("killall amxrt");
    system("killall ubusd");

    amxb_be_remove_all();

    printf("STOP\n");

    return 0;
}

void test_can_remove_proxy_object(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "proxy", "Device.IP.Diagnostics.");

    assert_int_equal(amxb_call(bus_ctx, "ProxyManager.", "unregister", &args, &ret, 5), 0);

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_add_proxy_object(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "proxy", "Device.IP.Diagnostics.");
    amxc_var_add_key(cstring_t, &args, "real", "IPDiagnostics.");
    assert_int_equal(amxb_call(bus_ctx, "ProxyManager.", "register", &args, &ret, 5), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "proxy", "Device.IP.");
    amxc_var_add_key(cstring_t, &args, "real", "IP.");
    assert_int_equal(amxb_call(bus_ctx, "ProxyManager.", "register", &args, &ret, 5), 0);

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_wrong_proxy_object_does_not_cause_segfault(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "proxy", "Device.NotExisting.1.");
    amxc_var_add_key(cstring_t, &args, "real", "IPDiagnostics.");
    assert_int_not_equal(amxb_call(bus_ctx, "ProxyManager.", "register", &args, &ret, 5), 0);

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_get_proxy_object_mappings(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxb_call(bus_ctx, "ProxyManager.", "list", &args, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_call_verify(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxb_call(bus_ctx, "ProxyManager.", "verify", &args, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    assert_string_equal(GETP_CHAR(&ret, "0.'IP.'"), "OK");
    assert_string_equal(GETP_CHAR(&ret, "0.'IPDiagnostics.'"), "OK");
    assert_string_equal(GETP_CHAR(&ret, "0.'MQTT.'"), "OK");
    assert_string_equal(GETP_CHAR(&ret, "0.'Unavailable.'"), "Not found");

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_call_verify_mapped(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "verify_mapped", true);
    assert_int_equal(amxb_call(bus_ctx, "ProxyManager.", "verify", &args, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    assert_string_equal(GETP_CHAR(&ret, "0.'Device.IP.'"), "OK");
    assert_string_equal(GETP_CHAR(&ret, "0.'Device.IP.Diagnostics.'"), "OK");
    assert_string_equal(GETP_CHAR(&ret, "0.'Device.MQTT.'"), "OK");
    assert_string_equal(GETP_CHAR(&ret, "0.'Device.Unavailable.'"), "Not responding");

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_call_verify_with_mappings(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* mappings = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    mappings = amxc_var_add_key(amxc_htable_t, &args, "mappings", NULL);
    amxc_var_add_key(cstring_t, mappings, "Device.MQTT.", "MQTT.");
    assert_int_equal(amxb_call(bus_ctx, "ProxyManager.", "verify", &args, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    assert_null(GETP_CHAR(&ret, "0.'IP.'"));
    assert_null(GETP_CHAR(&ret, "0.'IPDiagnostics.'"));
    assert_string_equal(GETP_CHAR(&ret, "0.'MQTT.'"), "OK");
    assert_null(GETP_CHAR(&ret, "0.'Unavailable.'"));

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
