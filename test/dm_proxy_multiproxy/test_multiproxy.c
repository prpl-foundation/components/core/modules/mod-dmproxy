/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <sys/signalfd.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "../../include_priv/dm_proxy.h"

#include <amxb/amxb_register.h>

#include "../mocks/dummy_be.h"
#include "../mocks/test_common.h"

#include "test_multiproxy.h"

static amxo_parser_t parser;
static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;

static amxd_status_t func(UNUSED amxd_object_t* obj,
                          UNUSED amxd_function_t* func,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    char* obj_path = amxd_object_get_path(obj, AMXD_OBJECT_TERMINATE);
    printf("Func called on %s\n", obj_path);
    free(obj_path);
    return amxd_status_ok;
}

int test_dm_proxy_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    amxo_parser_init(&parser);
    amxd_dm_init(&dm);

    amxo_resolver_ftab_add(&parser, "func", AMXO_FUNC(func));

    root_obj = amxd_dm_get_root(&dm);
    amxo_parser_parse_file(&parser, "../mocks/multiproxy.odl", root_obj);

    assert_int_equal(test_register_dummy_be(), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxb_register(bus_ctx, &dm);
    amxb_set_access(bus_ctx, amxd_dm_access_public);

    handle_events();

    assert_int_equal(_dm_proxy_main(AMXO_START, &dm, &parser), 0);

    return 0;
}

int test_dm_proxy_teardown(UNUSED void** state) {
    assert_int_equal(_dm_proxy_main(AMXO_STOP, &dm, &parser), 0);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}

void test_dm_proxy_get(UNUSED void** state) {
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_int_equal(amxb_get(bus_ctx, "Me.", INT32_MAX, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_non_null(GETP_ARG(&ret, "0.'Me.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.'.Me"));
    assert_string_equal(GETP_CHAR(&ret, "0.'Me.'.Me"), "Me");
    assert_non_null(GETP_ARG(&ret, "0.'Me.MappedToNothing.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.MeItems.1.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.MeItems.1.Plugins.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.MeItems.1.Plugins.'.Me"));
    assert_string_equal(GETP_CHAR(&ret, "0.'Me.MeItems.1.Plugins.'.Me"), "OtherItem1Plugin");
    assert_non_null(GETP_ARG(&ret, "0.'Me.MeItems.2.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.MeItems.2.Plugins.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.NoMappings.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.NoMappings.NotMappedItems.1.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.NoMappings.NotMappedItems.2.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.Plugins.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.Plugins.Mib.'"));

    assert_int_equal(amxb_get(bus_ctx, "Me.Plugins.", INT32_MAX, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_non_null(GETP_ARG(&ret, "0.'Me.Plugins.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.Plugins.'.Me"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.Plugins.Mib.'.MibString"));

    assert_int_equal(amxb_get(bus_ctx, "Me.NoMappings.", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_non_null(GETP_ARG(&ret, "0.'Me.NoMappings.'"));
    assert_null(GETP_ARG(&ret, "0.'Me.NoMappings.NotMappedItems.1.'"));

    assert_int_equal(amxb_get(bus_ctx, "Me.MappedToNothing.", INT32_MAX, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_non_null(GETP_ARG(&ret, "0.'Me.MappedToNothing.'"));

    amxc_var_clean(&ret);
}

void test_dm_proxy_gsdm(UNUSED void** state) {
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_int_equal(amxb_get_supported(bus_ctx, "Me.", AMXB_FLAG_FUNCTIONS | AMXB_FLAG_PARAMETERS | AMXB_FLAG_EVENTS, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_non_null(GETP_ARG(&ret, "0.'Me.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.MappedToNothing.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.MeItems.{i}.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.NoMappings.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.NoMappings.NotMappedItems.{i}.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.Plugins.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.Plugins.Mib.'"));

    assert_int_equal(amxb_get_supported(bus_ctx, "Me.", AMXB_FLAG_FIRST_LVL | AMXB_FLAG_FUNCTIONS | AMXB_FLAG_PARAMETERS | AMXB_FLAG_EVENTS, &ret, 5), 0);
    assert_non_null(GETP_ARG(&ret, "0.'Me.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.MappedToNothing.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.MeItems.{i}.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.NoMappings.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.Plugins.'"));
    amxc_var_dump(&ret, STDOUT_FILENO);

    assert_int_equal(amxb_get_supported(bus_ctx, "Me.NoMappings.", AMXB_FLAG_FUNCTIONS | AMXB_FLAG_PARAMETERS | AMXB_FLAG_EVENTS, &ret, 5), 0);
    assert_non_null(GETP_ARG(&ret, "0.'Me.NoMappings.'"));
    assert_non_null(GETP_ARG(&ret, "0.'Me.NoMappings.NotMappedItems.{i}.'"));
    amxc_var_dump(&ret, STDOUT_FILENO);

    amxc_var_clean(&ret);
}

void test_dm_proxy_rpc(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "method", "func");

    assert_int_equal(amxb_call(bus_ctx, "Me.Plugins.", "_exec", &args, &ret, 5), 0);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

void test_dm_proxy_set(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t params;

    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Me", "Test");
    assert_int_equal(amxb_set(bus_ctx, "Me.MeItems.1.Plugins.", &params, &ret, 5), 0);

    assert_int_equal(amxb_get(bus_ctx, "Me.MeItems.1.Plugins.", INT32_MAX, &ret, 5), 0);
    assert_non_null(GETP_ARG(&ret, "0.'Me.MeItems.1.Plugins.'.Me"));
    assert_string_equal(GETP_CHAR(&ret, "0.'Me.MeItems.1.Plugins.'.Me"), "Test");

    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}