/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "dm_proxy.h"
#include "dm_proxy_methods.h"
#include "dm_proxy_info.h"

#define ME "proxy"

static void SAH_TRACEZ_LOG_VARIANT(const amxc_var_t* var) {
    when_true(amxc_var_type_of(var) == AMXC_VAR_ID_NULL, exit);
    if(sahTraceLevel() >= 500) {
        switch(sahTraceType()) {
        case TRACE_TYPE_SYSLOG:
            amxc_var_log(var);
            break;
        case TRACE_TYPE_STDOUT:
            amxc_var_dump(var, STDOUT_FILENO);
            break;
        case TRACE_TYPE_STDERR:
            amxc_var_dump(var, STDERR_FILENO);
            break;
        default:
            break;
        }
    }

exit:
    return;
}

static amxd_object_t* dm_proxy_find_object(amxd_object_t* object, amxc_var_t* rel_path) {
    amxd_path_t path;
    amxc_string_t full_path;
    char* path_part = NULL;
    amxd_object_t* tmp = NULL;

    amxc_string_init(&full_path, 0);
    amxd_path_init(&path, GET_CHAR(rel_path, NULL));
    do {
        path_part = amxd_path_get_first(&path, true);
        tmp = amxd_object_findf(object, "%s", path_part);
        if(tmp != NULL) {
            free(path_part);
            object = tmp;
        } else {
            amxd_path_prepend(&path, path_part);
            free(path_part);
        }
    } while(tmp != NULL);

    amxc_string_setf(&full_path, "%s%s",
                     amxd_path_get(&path, AMXD_OBJECT_TERMINATE),
                     amxd_path_get_param(&path) == NULL? "":amxd_path_get_param(&path));
    amxc_var_set(cstring_t, rel_path, amxc_string_get(&full_path, 0));

    amxc_string_clean(&full_path);
    amxd_path_clean(&path);
    return object;
}

static void dm_proxy_reply_transl_path(proxy_info_t* pinfo,
                                       amxc_var_t* proxy_ret,
                                       amxc_var_t* orig_ret) {
    amxc_var_t* orig = GETI_ARG(orig_ret, 0);
    amxc_string_t proxy_path;
    const char* real_obj = dm_proxy_get_translate_path(pinfo->proxy_obj);

    amxc_var_set_type(proxy_ret, amxc_var_type_of(orig));

    amxc_string_init(&proxy_path, 256);

    amxc_var_for_each(reply_item, orig) {
        const char* key = amxc_var_key(reply_item);
        const char* proxy_key = NULL;
        char* stripped = dm_proxy_strip_mapped(real_obj, key);

        amxc_string_setf(&proxy_path, "%s%s", pinfo->proxy_obj, stripped);
        free(stripped);

        proxy_key = amxc_string_get(&proxy_path, 0);
        amxc_var_set_key(proxy_ret, proxy_key, reply_item, AMXC_VAR_FLAG_DEFAULT);
    }

    amxc_string_clean(&proxy_path);
}

static void dm_proxy_reply_transl_list(proxy_info_t* pinfo,
                                       amxc_var_t* proxy_ret,
                                       amxc_var_t* orig_ret) {
    amxc_var_t* orig = GETI_ARG(orig_ret, 0);
    amxc_string_t proxy_path;
    const char* real_obj = dm_proxy_get_translate_path(pinfo->proxy_obj);

    amxc_var_set_type(proxy_ret, amxc_var_type_of(orig));

    amxc_string_init(&proxy_path, 256);

    amxc_var_for_each(reply_item, orig) {
        const char* item = amxc_var_constcast(cstring_t, reply_item);
        const char* proxy_item = NULL;
        char* stripped = dm_proxy_strip_mapped(real_obj, item);

        amxc_string_setf(&proxy_path, "%s%s", pinfo->proxy_obj, stripped);
        free(stripped);

        proxy_item = amxc_string_get(&proxy_path, 0);
        amxc_var_add(cstring_t, proxy_ret, proxy_item);
    }

    amxc_string_clean(&proxy_path);
}

static void dm_proxy_rpc_done_transl_list(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                          UNUSED amxb_request_t* request,
                                          int status,
                                          void* priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;
    dm_proxy_reply_transl_list(pinfo, &pinfo->ret, request->result);
    SAH_TRACEZ_INFO(ME, "Forwarded request done - call id= %d", (uint32_t) pinfo->call_id);
    SAH_TRACEZ_INFO(ME, "Result = %d", status);
    SAH_TRACEZ_LOG_VARIANT(&pinfo->ret);
    amxd_function_deferred_call_done(pinfo->call_id, status, NULL, &pinfo->ret);

    dm_proxy_delete_info(&pinfo);
}

static void dm_proxy_rpc_done_transl_path(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                          UNUSED amxb_request_t* request,
                                          int status,
                                          void* priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;
    dm_proxy_reply_transl_path(pinfo, &pinfo->ret, request->result);
    SAH_TRACEZ_INFO(ME, "Forwarded request done - call id= %d", (uint32_t) pinfo->call_id);
    SAH_TRACEZ_INFO(ME, "Result = %d", status);
    SAH_TRACEZ_LOG_VARIANT(&pinfo->ret);
    amxd_function_deferred_call_done(pinfo->call_id, status, NULL, &pinfo->ret);

    dm_proxy_delete_info(&pinfo);
}

static void dm_proxy_rpc_done_transl_data(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                          UNUSED amxb_request_t* request,
                                          int status,
                                          void* priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;
    amxc_var_t* ret = GETI_ARG(pinfo->request->result, 0);
    amxc_var_t* local_objs = GET_ARG(&pinfo->ret, "objects");
    amxc_var_t* remote_objs = GET_ARG(ret, "objects");

    dm_proxy_reply_transl_data(pinfo, ret);
    SAH_TRACEZ_INFO(ME, "Forwarded request done - call id= %d", (uint32_t) pinfo->call_id);
    SAH_TRACEZ_INFO(ME, "Result = %d", status);
    amxc_var_for_each(obj, local_objs) {
        amxc_var_set_index(remote_objs, -1, obj, AMXC_VAR_FLAG_DEFAULT);
    }
    SAH_TRACEZ_LOG_VARIANT(ret);
    amxd_function_deferred_call_done(pinfo->call_id, status, NULL, ret);

    dm_proxy_delete_info(&pinfo);
}

static void dm_proxy_rpc_done_transl_data_multiple(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                                   UNUSED amxb_request_t* request,
                                                   int status,
                                                   void* priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;
    amxc_var_t* ret = GETI_ARG(pinfo->request->result, 0);

    if(amxc_var_type_of(ret) == AMXC_VAR_ID_LIST) {
        amxc_var_for_each(entry, ret) {
            dm_proxy_reply_transl_data(pinfo, entry);
        }
    } else {
        dm_proxy_reply_transl_data(pinfo, ret);
    }
    SAH_TRACEZ_INFO(ME, "Forwarded request done - call id= %d", (uint32_t) pinfo->call_id);
    SAH_TRACEZ_INFO(ME, "Result = %d", status);
    SAH_TRACEZ_LOG_VARIANT(&pinfo->ret);
    amxd_function_deferred_call_done(pinfo->call_id, status, NULL, ret);

    dm_proxy_delete_info(&pinfo);
}

static void dm_proxy_rpc_done(UNUSED const amxb_bus_ctx_t* bus_ctx,
                              UNUSED amxb_request_t* request,
                              int status,
                              void* priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;
    amxc_var_t* ret = GETI_ARG(pinfo->request->result, 0);
    amxc_var_t* out_args = GETI_ARG(pinfo->request->result, 1);

    SAH_TRACEZ_INFO(ME, "Forwarded request done - call id= %d", (uint32_t) pinfo->call_id);
    SAH_TRACEZ_INFO(ME, "Result = %d", status);
    SAH_TRACEZ_LOG_VARIANT(ret);
    SAH_TRACEZ_LOG_VARIANT(out_args);
    amxd_function_deferred_call_done(pinfo->call_id, status, out_args, ret);

    dm_proxy_delete_info(&pinfo);
}

static void dm_proxy_rpc_list_done(const amxb_bus_ctx_t* bus_ctx,
                                   amxb_request_t* request,
                                   int status,
                                   void* priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;
    amxc_var_t* objects = GETP_ARG(pinfo->request->result, "0.objects");

    if(objects != NULL) {
        amxd_object_t* obj = amxd_dm_findf(dm_proxy_get_dm(), "%s", pinfo->proxy_obj);
        amxd_object_for_each(child, it, obj) {
            amxd_object_t* child_obj = amxc_container_of(it, amxd_object_t, it);
            amxc_var_add(cstring_t, objects, amxd_object_get_name(child_obj, 0));
        }
    }

    SAH_TRACEZ_INFO(ME, "Forwarded request done - call id= %d", (uint32_t) pinfo->call_id);
    SAH_TRACEZ_INFO(ME, "Result = %d", status);

    dm_proxy_rpc_done(bus_ctx, request, status, priv);
}

static void dm_proxy_rpc_cancel(UNUSED uint64_t call_id, void* const priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;
    SAH_TRACEZ_NOTICE(ME, "Forwarded canceled - call id= %d", (uint32_t) call_id);

    dm_proxy_delete_info(&pinfo);
}

static void dm_proxy_set_rel_path(amxd_path_t* path, amxc_var_t* rel_path_var) {
    amxc_string_t rel_path;
    amxc_string_init(&rel_path, 0);

    if((amxd_path_get(path, 0) != NULL) && (amxd_path_get(path, 0)[0] != 0)) {
        if(amxd_path_get_param(path) != 0) {
            amxc_string_setf(&rel_path, "%s%s", amxd_path_get(path, AMXD_OBJECT_TERMINATE), amxd_path_get_param(path));
            amxc_var_set(cstring_t, rel_path_var, amxc_string_get(&rel_path, 0));
        } else {
            amxc_var_set(cstring_t, rel_path_var, amxd_path_get(path, AMXD_OBJECT_TERMINATE));
        }
    } else {
        if(amxd_path_get_param(path) != 0) {
            amxc_var_set(cstring_t, rel_path_var, amxd_path_get_param(path));
        } else {
            amxc_var_delete(&rel_path_var);
        }
    }

    amxc_string_clean(&rel_path);
}

static amxd_status_t dm_proxy_rpc_forward(proxy_info_t* pinfo,
                                          amxd_object_t* object,
                                          amxd_function_t* func,
                                          amxc_var_t* args,
                                          amxb_be_done_cb_fn_t done_fn) {
    amxd_status_t retval = amxd_status_object_not_found;
    char* object_path = amxd_object_get_path(object, AMXD_OBJECT_TERMINATE | AMXD_OBJECT_INDEXED);
    amxc_var_t* rel_path_var = GET_ARG(args, "rel_path");
    const char* fname = amxd_function_get_name(func);
    const char* pobj = NULL;
    char* robj = NULL;
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_path_t path;

    SAH_TRACEZ_IN(ME);
    amxd_path_init(&path, NULL);

    amxd_path_setf(&path, false, "%s%s", object_path, rel_path_var == NULL? "":GET_CHAR(rel_path_var, NULL));
    pobj = dm_proxy_get_real_path(&path);
    when_null(pobj, exit);
    robj = amxd_path_get_fixed_part(&path, true);
    bus_ctx = amxb_be_who_has(robj);
    if(rel_path_var == NULL) {
        rel_path_var = amxc_var_add_key(cstring_t, args, "rel_path", "");
    }
    dm_proxy_set_rel_path(&path, rel_path_var);

    if(bus_ctx != NULL) {
        amxc_var_t copy_args;
        SAH_TRACEZ_INFO(ME, "Forwarding request on bus ctx [%p] - %s", (void*) bus_ctx, bus_ctx->bus_fn->name);
        amxc_var_init(&copy_args);
        amxc_var_copy(&copy_args, args);
        pinfo->proxy_obj = pobj;
        SAH_TRACEZ_INFO(ME, "Forwarding - origin object = [%s], remote object = [%s], function = [%s]",
                        object_path, robj, fname);
        SAH_TRACEZ_LOG_VARIANT(&copy_args);

        pinfo->request = amxb_async_call(bus_ctx, robj, fname, &copy_args, done_fn, pinfo);
        amxc_var_clean(&copy_args);
        if(pinfo->request == NULL) {
            SAH_TRACEZ_ERROR(ME, "Forwarding failed - return error %d", amxd_status_unknown_error);
            retval = amxd_status_unknown_error;
            goto exit;
        }
        retval = amxd_status_deferred;
    } else {
        SAH_TRACEZ_ERROR(ME, "No bus ctx found for [%s]", robj);
    }

exit:
    free(object_path);
    free(robj);
    amxd_path_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static void dm_proxy_child_request_done(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                        UNUSED amxb_request_t* request,
                                        int status,
                                        void* priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;
    proxy_info_t* main_req = amxc_container_of(pinfo->it.llist, proxy_info_t, child_requests);

    SAH_TRACEZ_INFO(ME, "Forwarded child request done for call id = %d", (uint32_t) main_req->call_id);
    dm_proxy_reply_transl_path(pinfo, &pinfo->ret, request->result);
    amxc_var_for_each(rv, &pinfo->ret) {
        amxc_var_set_key(&main_req->ret, amxc_var_key(rv), rv, AMXC_VAR_FLAG_DEFAULT | AMXC_VAR_FLAG_UPDATE);
    }
    dm_proxy_delete_info(&pinfo);

    status = (status == amxd_status_object_not_found)? amxd_status_ok:status;
    if(amxc_llist_is_empty(&main_req->child_requests)) {
        SAH_TRACEZ_INFO(ME, "All child requests done for call id= %d", (uint32_t) main_req->call_id);
        SAH_TRACEZ_INFO(ME, "Final result = %d", status);
        SAH_TRACEZ_LOG_VARIANT(&main_req->ret);
        amxd_function_deferred_call_done(main_req->call_id, status, NULL, &main_req->ret);
        dm_proxy_delete_info(&main_req);
    }
}

static void dm_proxy_check_tree(amxd_object_t* const object,
                                int32_t depth,
                                void* priv) {
    rpc_data_t* data = (rpc_data_t*) priv;
    SAH_TRACEZ_IN(ME);
    if(dm_proxy_is_mapped(object, "")) {
        amxd_status_t retval = amxd_status_ok;
        proxy_info_t* pinfo = NULL;
        amxc_var_t* depth_var = GET_ARG(data->args, "depth");
        char* path = amxd_object_get_path(object, AMXD_OBJECT_TERMINATE | AMXD_OBJECT_INDEXED);
        amxc_var_set(int32_t, depth_var, depth);
        dm_proxy_new_info(&pinfo, path);
        amxc_llist_append(&data->main->child_requests, &pinfo->it);
        retval = dm_proxy_rpc_forward(pinfo, object, data->func, data->args, dm_proxy_child_request_done);
        if(retval == amxd_status_object_not_found) {
            dm_proxy_delete_info(&pinfo);
        }
        free(path);
    }
    SAH_TRACEZ_OUT(ME);
}

static int32_t dm_proxy_get_request_depth(const amxc_var_t* const args) {
    int32_t depth = GET_INT32(args, "depth");
    if(GET_ARG(args, "first_level_only") != NULL) {
        if(GET_BOOL(args, "first_level_only")) {
            depth = 0;
        } else {
            depth = INT32_MAX;
        }
    }

    if(depth == -1) {
        depth = INT32_MAX;
    }

    return depth;
}

static void dm_proxy_rpc_timeout_reply(UNUSED amxp_timer_t* timer, void* priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;

    SAH_TRACEZ_WARNING(ME, "Wait timer expired for call id = %d, forwarding recieved data", (uint32_t) pinfo->call_id);
    amxd_function_deferred_call_done(pinfo->call_id, 0, NULL, &pinfo->ret);

    dm_proxy_delete_info(&pinfo);
}

static void dm_proxy_rpc_timeout_fail(UNUSED amxp_timer_t* timer, void* priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;

    SAH_TRACEZ_WARNING(ME, "Wait timer expired for call id = %d, fail", (uint32_t) pinfo->call_id);
    amxd_function_deferred_call_done(pinfo->call_id, amxd_status_timeout, NULL, NULL);

    dm_proxy_delete_info(&pinfo);
}

static void dm_proxy_start_wait_timer(const char* func, proxy_info_t* pinfo) {
    mod_proxy_t* proxy = dm_proxy_get_info();
    const amxc_var_t* var_wait_time = GET_ARG(&proxy->parser->config, "proxy-wait-time");
    uint32_t wait_time = var_wait_time == NULL? 3:GET_UINT32(var_wait_time, NULL);

    SAH_TRACEZ_INFO(ME, "Starting wait timer for call id= %d", (uint32_t) pinfo->call_id);
    if((strcmp(func, "_get") == 0) ||
       (strcmp(func, "_get_supported") == 0) ||
       (strcmp(func, "_get_instances") == 0)) {
        amxp_timer_new(&pinfo->wait_timer, dm_proxy_rpc_timeout_reply, pinfo);
    } else if(strcmp(func, "_exec") != 0) {
        amxp_timer_new(&pinfo->wait_timer, dm_proxy_rpc_timeout_fail, pinfo);
    }

    if(pinfo->wait_timer != NULL) {
        amxp_timer_start(pinfo->wait_timer, wait_time * 1000);
    }
}

static amxd_status_t dm_proxy_check_object(amxd_object_t* object,
                                           amxd_function_t* func,
                                           amxc_var_t* args,
                                           amxc_var_t* ret,
                                           amxb_be_done_cb_fn_t done_fn) {
    amxd_status_t retval = amxd_status_object_not_found;
    int32_t depth = dm_proxy_get_request_depth(args);
    proxy_info_t* pinfo = NULL;
    rpc_data_t data;

    SAH_TRACEZ_IN(ME);
    retval = amxd_function_call_base(func, object, args, ret);
    when_failed(retval, exit);

    dm_proxy_new_info(&pinfo, NULL);
    pinfo->done_fn = done_fn;
    data.main = pinfo;
    data.func = func;
    data.args = args;

    amxc_var_move(&pinfo->ret, ret);
    amxd_object_hierarchy_walk(object, amxd_direction_down, NULL, dm_proxy_check_tree, depth, &data);
    if(amxc_llist_is_empty(&pinfo->child_requests)) {
        amxc_var_move(ret, &pinfo->ret);
        dm_proxy_delete_info(&pinfo);
    } else {
        amxd_function_defer(func, &pinfo->call_id, ret, dm_proxy_rpc_cancel, pinfo);
        SAH_TRACEZ_INFO(ME, "Deferred call id = %d (private data = %p)", (uint32_t) pinfo->call_id, (void*) pinfo);
        dm_proxy_start_wait_timer(amxd_function_get_name(func), pinfo);
        retval = amxd_status_deferred;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static amxd_status_t dm_proxy_check_resolved(amxd_object_t* object,
                                             amxd_function_t* func,
                                             amxc_var_t* args,
                                             amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_object_not_found;
    const char* rel_path = GET_CHAR(args, "rel_path");
    amxd_path_t input_path;
    amxc_llist_t paths;

    SAH_TRACEZ_IN(ME);
    amxd_path_init(&input_path, rel_path);
    amxc_llist_init(&paths);

    SAH_TRACEZ_INFO(ME, "Resolving path = [%s]", amxd_path_get(&input_path, 0));
    amxd_object_resolve_pathf(object, &paths, "%s", amxd_path_get(&input_path, 0));
    if(!amxc_llist_is_empty(&paths)) {
        amxc_llist_for_each(it, &paths) {
            amxc_string_t* path = amxc_string_from_llist_it(it);
            amxd_object_t* obj = amxd_dm_findf(dm_proxy_get_dm(), "%s", amxc_string_get(path, 0));
            SAH_TRACEZ_INFO(ME, "Matching object = [%s]", amxc_string_get(path, 0));
            if(dm_proxy_is_mapped(obj, "")) {
                if((rel_path == NULL) || (*rel_path == 0)) {
                    amxd_function_call_base(func, object, args, ret);
                    SAH_TRACEZ_LOG_VARIANT(ret);
                }
                amxc_llist_clean(&paths, amxc_string_list_it_free);
                amxd_path_clean(&input_path);
                goto exit;
            }
        }
        retval = amxd_function_call_base(func, object, args, ret);
        amxc_llist_clean(&paths, amxc_string_list_it_free);
    }

    amxd_path_clean(&input_path);

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static amxd_status_t dm_proxy_rpc_impl(amxd_object_t* object,
                                       amxd_function_t* func,
                                       amxc_var_t* args,
                                       amxc_var_t* ret,
                                       amxb_be_done_cb_fn_t done_fn) {
    amxd_status_t retval = amxd_status_object_not_found;
    proxy_info_t* pinfo = NULL;
    amxc_var_t* var_rel_path = GET_ARG(args, "rel_path");
    const char* rel_path = GET_CHAR(var_rel_path, NULL);
    int32_t depth = dm_proxy_get_request_depth(args);

    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "Recieved function call [%s] - object = [%s]:",
                    amxd_function_get_name(func),
                    amxd_object_get_name(object, AMXD_OBJECT_NAMED));
    SAH_TRACEZ_LOG_VARIANT(args);

    if((rel_path != NULL) && (*rel_path != 0)) {
        amxd_object_t* tmp = dm_proxy_find_object(object, var_rel_path);
        if(tmp != NULL) {
            object = tmp;
            rel_path = GET_CHAR(var_rel_path, NULL);
        }
    }

    if(((rel_path == NULL) || (*rel_path == 0)) && (depth > 0)) {
        retval = dm_proxy_check_object(object, func, args, ret, done_fn);
    } else {
        retval = dm_proxy_check_resolved(object, func, args, ret);
    }

    if(retval != amxd_status_object_not_found) {
        goto exit;
    }

    dm_proxy_new_info(&pinfo, NULL);
    amxc_var_move(&pinfo->ret, ret);
    retval = dm_proxy_rpc_forward(pinfo, object, func, args, done_fn);
    if(retval != amxd_status_deferred) {
        dm_proxy_delete_info(&pinfo);
        goto exit;
    }
    retval = amxd_function_defer(func, &pinfo->call_id, ret, dm_proxy_rpc_cancel, pinfo);
    if(retval == amxd_status_ok) {
        dm_proxy_start_wait_timer(amxd_function_get_name(func), pinfo);
        retval = amxd_status_deferred;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

amxd_status_t dm_proxy_rpc_transl_path(amxd_object_t* object,
                                       amxd_function_t* func,
                                       amxc_var_t* args,
                                       amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_object_not_found;

    retval = dm_proxy_rpc_impl(object, func, args, ret, dm_proxy_rpc_done_transl_path);

    return retval;
}

amxd_status_t dm_proxy_rpc_transl_data(amxd_object_t* object,
                                       amxd_function_t* func,
                                       amxc_var_t* args,
                                       amxc_var_t* ret) {

    amxd_status_t retval = amxd_status_object_not_found;

    retval = dm_proxy_rpc_impl(object, func, args, ret, dm_proxy_rpc_done_transl_data);

    return retval;
}

amxd_status_t dm_proxy_rpc_transl_data_multiple(amxd_object_t* object,
                                                amxd_function_t* func,
                                                amxc_var_t* args,
                                                amxc_var_t* ret) {

    amxd_status_t retval = amxd_status_object_not_found;

    retval = dm_proxy_rpc_impl(object, func, args, ret, dm_proxy_rpc_done_transl_data_multiple);

    return retval;
}

amxd_status_t dm_proxy_rpc(amxd_object_t* object,
                           amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret) {

    amxd_status_t retval = amxd_status_object_not_found;

    retval = dm_proxy_rpc_impl(object, func, args, ret, dm_proxy_rpc_done);

    return retval;
}

amxd_status_t dm_proxy_rpc_list(amxd_object_t* object,
                                amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {

    amxd_status_t retval = amxd_status_object_not_found;

    retval = dm_proxy_rpc_impl(object, func, args, ret, dm_proxy_rpc_list_done);

    return retval;
}

amxd_status_t dm_proxy_rpc_del(amxd_object_t* object,
                               amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {

    amxd_status_t retval = amxd_status_object_not_found;

    retval = dm_proxy_rpc_impl(object, func, args, ret, dm_proxy_rpc_done_transl_list);

    return retval;
}
