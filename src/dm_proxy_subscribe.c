/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "dm_proxy.h"
#include "dm_proxy_methods.h"
#include "dm_proxy_info.h"

#define ME "proxy"

static amxc_llist_t subscriptions;

static void dm_proxy_remove_subscription(amxc_llist_it_t* it) {
    amxb_subscription_t* subscription = amxc_container_of(it, amxb_subscription_t, it);
    proxy_info_t* pinfo = (proxy_info_t*) subscription->priv;
    if(pinfo != NULL) {
        pinfo->refs = 1;
        dm_proxy_delete_info(&pinfo);
    }

    amxc_llist_it_take(&subscription->it);
    amxb_subscription_delete(&subscription);
}

static void dm_proxy_handle_event(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv) {
    proxy_info_t* pinfo = (proxy_info_t*) priv;
    amxc_var_t proxy_data;
    amxd_dm_t* dm = dm_proxy_get_dm();
    const char* notification = GET_CHAR(data, "notification");

    amxc_var_init(&proxy_data);

    SAH_TRACEZ_INFO(ME, "Recieved event [%s] - translate and forward", notification);
    amxc_var_copy(&proxy_data, data);
    dm_proxy_reply_transl_data(pinfo, &proxy_data);

    amxp_sigmngr_trigger_signal(&dm->sigmngr, notification, &proxy_data);

    amxc_var_clean(&proxy_data);
}

static void dm_proxy_create_subscription(UNUSED const char* const sig_name,
                                         UNUSED const amxc_var_t* const data,
                                         void* const priv) {
    amxc_var_t* args = (amxc_var_t*) priv;
    amxc_var_t ret;
    amxd_dm_t* dm = dm_proxy_get_dm();
    const char* proxy_obj_path = GET_CHAR(args, "proxy-obj");
    amxd_object_t* proxy_obj = amxd_dm_get_object(dm, proxy_obj_path);

    amxc_var_init(&ret);
    amxd_object_invoke_function(proxy_obj, "_subscribe", args, &ret);

    amxc_var_clean(&ret);
    amxc_var_delete(&args);
}

static amxb_bus_ctx_t* dm_proxy_find_bus_ctx(amxd_path_t* path, amxc_var_t* args) {
    amxb_bus_ctx_t* ctx = NULL;
    char* fp = amxd_path_get_fixed_part(path, false);
    amxc_string_t signal;
    amxc_var_t* sub_args = NULL;

    amxc_string_init(&signal, 0);
    ctx = amxb_be_who_has(fp);
    if(ctx != NULL) {
        goto exit;
    }

    amxc_string_setf(&signal, "wait:%s", fp);
    amxc_var_new(&sub_args);
    amxc_var_move(sub_args, args);

    amxb_wait_for_object(fp);
    amxp_slot_connect(NULL, amxc_string_get(&signal, 0), NULL,
                      dm_proxy_create_subscription, sub_args);

exit:
    amxc_string_clean(&signal);
    free(fp);
    return ctx;
}

static amxb_subscription_t* dm_proxy_find_subscription(UNUSED amxb_bus_ctx_t* bus_ctx,
                                                       amxd_path_t* path) {
    amxb_subscription_t* subscription = NULL;
    char* sub_path = NULL;

    sub_path = amxd_path_get_fixed_part(path, false);
    if((sub_path != NULL) && (*sub_path != 0)) {
        sub_path[strlen(sub_path) - 1] = 0;
    }

    amxc_llist_iterate(it, &subscriptions) {
        subscription = amxc_container_of(it, amxb_subscription_t, it);
        if((sub_path != NULL) && (strcmp(subscription->object, sub_path) == 0)) {
            break;
        }
        subscription = NULL;
    }

    if(subscription != NULL) {
        goto exit;
    }

    subscription = amxb_subscription_find_parent(bus_ctx, amxd_path_get(path, 0));

exit:
    free(sub_path);
    return subscription;
}

static amxd_status_t dm_proxy_subscription(const char* proxy_obj,
                                           amxc_var_t* args,
                                           amxd_path_t* path) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_status_t retval = amxd_status_ok;
    amxb_subscription_t* subscription = NULL;
    proxy_info_t* pinfo = NULL;

    bus_ctx = dm_proxy_find_bus_ctx(path, args);
    when_null(bus_ctx, exit);
    subscription = dm_proxy_find_subscription(bus_ctx, path);
    if(subscription != NULL) {
        pinfo = (proxy_info_t*) subscription->priv;
        dm_proxy_info_refs_inc(pinfo, 1);
        goto exit;
    }

    dm_proxy_new_info(&pinfo, proxy_obj);

    retval = amxb_subscription_new(&subscription, bus_ctx, amxd_path_get(path, AMXD_OBJECT_TERMINATE),
                                   NULL, dm_proxy_handle_event, pinfo);
    when_null(subscription, exit);
    amxc_llist_append(&subscriptions, &subscription->it);

    subscription = amxb_subscription_find_child(bus_ctx, amxd_path_get(path, AMXD_OBJECT_TERMINATE));
    while(subscription != NULL) {
        proxy_info_t* pinfo_child = (proxy_info_t*) subscription->priv;
        dm_proxy_info_refs_inc(pinfo, pinfo_child->refs);
        dm_proxy_remove_subscription(&subscription->it);
        subscription = amxb_subscription_find_child(bus_ctx, amxd_path_get(path, AMXD_OBJECT_TERMINATE));
    }

exit:
    return retval;
}

static void dm_proxy_check_tree(amxd_object_t* const object,
                                UNUSED int32_t depth,
                                void* priv) {
    rpc_data_t* data = (rpc_data_t*) priv;
    SAH_TRACEZ_IN(ME);
    if(dm_proxy_is_mapped(object, "")) {
        amxd_path_t path;
        const char* pobj = NULL;
        char* object_path = amxd_object_get_path(object, AMXD_OBJECT_TERMINATE | AMXD_OBJECT_INDEXED);

        amxd_path_init(&path, NULL);

        amxd_path_setf(&path, false, "%s", object_path);
        pobj = dm_proxy_get_real_path(&path);
        amxc_var_add_key(cstring_t, data->args, "proxy-obj", object_path);

        SAH_TRACEZ_INFO(ME, "Subscribe: origin [%s] - remote [%s]", object_path, pobj);
        dm_proxy_subscription(pobj, data->args, &path);

        free(object_path);
        amxd_path_clean(&path);
    }
    SAH_TRACEZ_OUT(ME);
}

amxd_status_t dm_proxy_subscribe(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_ok;
    char* object_path = amxd_object_get_path(object, AMXD_OBJECT_TERMINATE | AMXD_OBJECT_INDEXED);
    amxc_var_t* rel_path_var = GET_ARG(args, "rel_path");
    amxd_path_t path;
    const char* pobj = NULL;

    amxd_path_init(&path, NULL);

    amxd_path_setf(&path, false, "%s%s", object_path, GET_CHAR(rel_path_var, NULL));
    SAH_TRACEZ_INFO(ME, "Add subscription for [%s]", amxd_path_get(&path, AMXD_OBJECT_TERMINATE));
    pobj = dm_proxy_get_real_path(&path);
    amxc_var_add_key(cstring_t, args, "proxy-obj", object_path);

    if((pobj == NULL) || (*pobj == 0)) {
        rpc_data_t data;
        data.main = NULL;
        data.func = NULL;
        data.args = args;

        amxd_object_hierarchy_walk(object, amxd_direction_down, NULL, dm_proxy_check_tree, INT32_MAX, &data);
    } else {
        retval = dm_proxy_subscription(pobj, args, &path);
    }

    free(object_path);
    amxd_path_clean(&path);
    return retval;
}

amxd_status_t dm_proxy_unsubscribe(UNUSED amxd_object_t* object,
                                   UNUSED amxd_function_t* func,
                                   amxc_var_t* args,
                                   UNUSED amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_ok;
    amxb_bus_ctx_t* bus_ctx = NULL;
    char* object_path = amxd_object_get_path(object, AMXD_OBJECT_TERMINATE | AMXD_OBJECT_INDEXED);
    amxc_var_t* rel_path_var = GET_ARG(args, "rel_path");
    char* fp = NULL;
    amxd_path_t path;

    amxd_path_init(&path, NULL);
    amxd_path_setf(&path, false, "%s%s", object_path, GET_CHAR(rel_path_var, NULL));
    SAH_TRACEZ_INFO(ME, "Remove subscription for [%s]", amxd_path_get(&path, AMXD_OBJECT_TERMINATE));
    dm_proxy_get_real_path(&path);

    fp = amxd_path_get_fixed_part(&path, false);
    if((fp != NULL) && (*fp != 0)) {
        fp[strlen(fp) - 1] = 0;
    }

    bus_ctx = amxb_be_who_has(fp);
    if(bus_ctx != NULL) {
        if(fp != NULL) {
            amxb_subscription_t* subscription = NULL;
            amxc_llist_iterate(it, &subscriptions) {
                subscription = amxc_container_of(it, amxb_subscription_t, it);
                if(strcmp(subscription->object, fp) == 0) {
                    break;
                }
                subscription = NULL;
            }
            if(subscription != NULL) {
                proxy_info_t* pinfo = (proxy_info_t*) subscription->priv;
                if(dm_proxy_delete_info(&pinfo) == 0) {
                    subscription->priv = NULL;
                    dm_proxy_remove_subscription(&subscription->it);
                }
            }
        }
    }

    free(fp);
    free(object_path);
    amxd_path_clean(&path);
    return retval;
}

void dm_proxy_remove_subscriptions(void) {
    amxc_llist_clean(&subscriptions, dm_proxy_remove_subscription);
}