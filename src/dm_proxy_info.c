/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "dm_proxy.h"
#include "dm_proxy_info.h"

static void dm_proxy_delete_child_request(amxc_llist_it_t* it) {
    proxy_info_t* info = amxc_container_of(it, proxy_info_t, it);
    dm_proxy_delete_info(&info);
}

void dm_proxy_reply_transl_data(proxy_info_t* pinfo, amxc_var_t* data) {
    const char* real_obj = dm_proxy_get_translate_path(pinfo->proxy_obj);
    amxd_object_t* obj = amxd_dm_findf(dm_proxy_get_dm(), "%s", pinfo->proxy_obj);
    amxc_var_t* eobject = GET_ARG(data, "eobject");
    amxc_var_t* object = GET_ARG(data, "object");
    amxc_var_t* path = GET_ARG(data, "path");
    amxc_var_t* name = GET_ARG(data, "name");
    char* stripped = NULL;
    char* p = NULL;

    amxc_string_t proxy_path;
    amxc_string_init(&proxy_path, 128);

    stripped = dm_proxy_strip_mapped(real_obj, GET_CHAR(path, NULL));
    amxc_string_setf(&proxy_path, "%s%s", pinfo->proxy_obj, stripped);
    if((name != NULL) && (stripped != NULL) && (*stripped == 0)) {
        amxc_var_set(cstring_t, name, amxd_object_get_name(obj, 0));
    }
    free(stripped);
    amxc_var_set(cstring_t, path, amxc_string_get(&proxy_path, 0));

    stripped = dm_proxy_strip_mapped(real_obj, GET_CHAR(object, NULL));
    p = amxd_object_get_path(obj, AMXD_OBJECT_TERMINATE);
    amxc_string_setf(&proxy_path, "%s%s", p, stripped);
    free(stripped);
    free(p);
    amxc_var_set(cstring_t, object, amxc_string_get(&proxy_path, 0));

    if(eobject != NULL) {
        stripped = dm_proxy_strip_mapped(real_obj, GET_CHAR(eobject, NULL));
        p = amxd_object_get_path(obj, AMXD_OBJECT_TERMINATE | AMXD_OBJECT_EXTENDED);
        amxc_string_setf(&proxy_path, "%s%s", p, stripped);
        free(stripped);
        free(p);
        amxc_var_set(cstring_t, eobject, amxc_string_get(&proxy_path, 0));
    }

    amxc_string_clean(&proxy_path);
}

int dm_proxy_new_info(proxy_info_t** pinfo, const char* proxy_obj) {
    int retval = -1;

    *pinfo = (proxy_info_t*) calloc(1, sizeof(proxy_info_t));
    when_null(*pinfo, exit);

    (*pinfo)->proxy_obj = proxy_obj;
    (*pinfo)->refs = 1;

    amxc_llist_init(&(*pinfo)->child_requests);
    amxc_var_init(&(*pinfo)->ret);

    retval = 0;

exit:
    return retval;
}

int dm_proxy_delete_info(proxy_info_t** pinfo) {
    int retval = --(*pinfo)->refs;

    if((*pinfo)->refs <= 0) {
        amxb_close_request(&(*pinfo)->request);
        amxc_var_clean(&(*pinfo)->ret);
        amxc_llist_it_take(&(*pinfo)->it);
        amxc_llist_clean(&(*pinfo)->child_requests, dm_proxy_delete_child_request);
        if((*pinfo)->wait_timer != NULL) {
            amxp_timer_delete(&(*pinfo)->wait_timer);
            // garbage collect deleted timers
            amxp_timers_check();
        }
        free(*pinfo);
        *pinfo = NULL;
    }

    return retval;
}

void dm_proxy_info_refs_inc(proxy_info_t* pinfo, int refs) {
    pinfo->refs += refs;
}
