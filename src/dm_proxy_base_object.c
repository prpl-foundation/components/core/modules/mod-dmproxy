/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "dm_proxy.h"
#include "dm_proxy_methods.h"

static amxd_function_t* dm_proxy_add_func(amxd_object_t* object,
                                          const char* name,
                                          amxd_object_fn_t fn) {
    amxd_status_t retval = amxd_status_ok;
    amxd_function_t* func = NULL;
    amxc_var_t def_val;

    amxc_var_init(&def_val);

    retval = amxd_function_new(&func, name, AMXC_VAR_ID_ANY, fn);
    when_failed(retval, exit);
    amxd_function_set_attr(func, amxd_fattr_template, true);
    amxd_function_set_attr(func, amxd_fattr_instance, true);
    amxd_function_set_attr(func, amxd_fattr_protected, true);

    amxc_var_set(cstring_t, &def_val, "");
    retval = amxd_function_new_arg(func, "rel_path", AMXC_VAR_ID_CSTRING, &def_val);
    amxd_function_arg_set_attr(func, "rel_path", amxd_aattr_in, true);
    amxd_function_arg_set_attr(func, "rel_path", amxd_aattr_strict, true);
    when_failed(retval, exit);

    retval = amxd_object_add_function(object, func);

exit:
    if(retval != amxd_status_ok) {
        amxd_function_delete(&func);
    }
    amxc_var_clean(&def_val);
    return func;
}

static amxd_function_t* dm_proxy_add_subscribe_func(amxd_object_t* object) {
    amxd_function_t* func = dm_proxy_add_func(object, "_subscribe", dm_proxy_subscribe);
    amxc_var_t def_val;

    amxc_var_init(&def_val);
    amxc_var_set(cstring_t, &def_val, "");

    when_null(func, exit);

    amxd_function_new_arg(func, "expression", AMXC_VAR_ID_CSTRING, &def_val);
    amxd_function_arg_set_attr(func, "expression", amxd_aattr_in, true);
    amxd_function_arg_set_attr(func, "expression", amxd_aattr_strict, true);
    amxd_function_new_arg(func, "rel_path", AMXC_VAR_ID_CSTRING, &def_val);
    amxd_function_arg_set_attr(func, "rel_path", amxd_aattr_in, true);
    amxd_function_arg_set_attr(func, "rel_path", amxd_aattr_strict, true);

exit:
    amxc_var_clean(&def_val);
    return func;
}

void dm_proxy_object_create(void) {
    mod_proxy_t* proxy = dm_proxy_get_info();

    // no need to delete the proxy_base object.
    // this is done automatically when it's base object is deleted
    amxd_object_new(&proxy->proxy_base, amxd_object_singleton, "proxy_base");

    amxd_object_change_function(proxy->proxy_base, "_get", dm_proxy_rpc_transl_path);
    amxd_object_change_function(proxy->proxy_base, "_set", dm_proxy_rpc_transl_path);
    amxd_object_change_function(proxy->proxy_base, "_add", dm_proxy_rpc_transl_data_multiple);
    amxd_object_change_function(proxy->proxy_base, "_del", dm_proxy_rpc_del);
    amxd_object_change_function(proxy->proxy_base, "_list", dm_proxy_rpc_list);
    amxd_object_change_function(proxy->proxy_base, "_describe", dm_proxy_rpc_transl_data);
    amxd_object_change_function(proxy->proxy_base, "_exec", dm_proxy_rpc);
    amxd_object_change_function(proxy->proxy_base, "_get_supported", dm_proxy_rpc_transl_path);
    amxd_object_change_function(proxy->proxy_base, "_get_instances", dm_proxy_rpc_transl_path);

    dm_proxy_add_subscribe_func(proxy->proxy_base);
    dm_proxy_add_func(proxy->proxy_base, "_unsubscribe", dm_proxy_unsubscribe);
}

void dm_proxy_object_delete(void) {
    mod_proxy_t* proxy = dm_proxy_get_info();

    amxd_object_free(&proxy->proxy_base);
}