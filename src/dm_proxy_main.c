/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dm_proxy.h"
#include "dm_proxy_methods.h"

static mod_proxy_t proxy;

static int isdot(int c) {
    return (c == '.') ? 1 : 0;
}

static int dm_proxy_add_object(amxd_dm_t* dm, const char* path) {
    int retval = -1;
    amxc_string_t obj_path;
    amxc_llist_t parts;
    amxd_object_t* object = amxd_dm_get_root(dm);

    amxc_llist_init(&parts);
    amxc_string_init(&obj_path, 0);
    amxc_string_setf(&obj_path, "%s", path);
    amxc_string_trimr(&obj_path, isdot);
    amxc_string_split_to_llist(&obj_path, &parts, '.');

    amxc_llist_for_each(it, &parts) {
        amxc_string_t* obj_name = amxc_string_from_llist_it(it);
        amxd_object_t* obj = amxd_object_get(object, amxc_string_get(obj_name, 0));
        if(obj != NULL) {
            amxc_llist_append(&proxy.proxy_base->derived_objects, &obj->derived_from);
            object = obj;
            continue;
        }
        if((amxd_object_get_type(object) == amxd_object_root) && (obj == NULL)) {
            goto exit;
        }
        amxd_object_new(&obj, amxd_object_singleton, amxc_string_get(obj_name, 0));
        when_null(obj, exit);
        amxc_llist_append(&proxy.proxy_base->derived_objects, &obj->derived_from);
        amxd_object_add_object(object, obj);
        object = obj;
    }

    retval = 0;

exit:
    amxc_llist_clean(&parts, amxc_string_list_it_free);
    amxc_string_clean(&obj_path);
    return retval;
}

static int dm_proxy_remove_object(amxd_dm_t* dm, amxd_object_t* proxy_obj) {
    while(proxy_obj != NULL) {
        amxd_object_t* parent = amxd_object_get_parent(proxy_obj);
        if(amxc_llist_is_empty(&proxy_obj->objects) &&
           amxc_llist_is_empty(&proxy_obj->parameters) &&
           amxc_llist_is_empty(&proxy_obj->derived_objects) &&
           !dm_proxy_is_mapped(proxy_obj, NULL) &&
           (parent != amxd_dm_get_root(dm))) {
            amxd_object_send_signal(proxy_obj, "dm:object-removed", NULL, false);
            amxd_object_free(&proxy_obj);
        } else {
            break;
        }
        proxy_obj = parent;
    }

    return 0;
}

static int dm_proxy_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = -1;
    const amxc_var_t* config_po = GET_ARG(&parser->config, "proxy-object");
    const amxc_htable_t* proxy_objs = NULL;
    amxd_object_t* proxy_obj = NULL;
    amxd_object_t* parent = NULL;

    proxy.dm = dm;
    proxy.parser = parser;

    if(config_po == NULL) {
        config_po = amxc_var_add_key(amxc_htable_t, &parser->config, "proxy-object", NULL);
    }

    dm_proxy_object_create();

    when_false(amxc_var_type_of(config_po) == AMXC_VAR_ID_HTABLE, exit);
    proxy_objs = amxc_var_constcast(amxc_htable_t, config_po);
    proxy.proxy_paths = amxc_htable_get_sorted_keys(proxy_objs);

    amxc_var_for_each(path, config_po) {
        proxy_obj = amxd_dm_findf(dm, "%s", amxc_var_key(path));
        if(proxy_obj != NULL) {
            amxc_llist_append(&proxy.proxy_base->derived_objects, &proxy_obj->derived_from);
            parent = amxd_object_get_parent(proxy_obj);
            while(parent != NULL && amxd_object_get_type(parent) != amxd_object_root) {
                if(parent->derived_from.llist != &proxy.proxy_base->derived_objects) {
                    amxc_llist_append(&proxy.proxy_base->derived_objects, &parent->derived_from);
                }
                parent = amxd_object_get_parent(parent);
            }
        } else {
            retval = dm_proxy_add_object(dm, amxc_var_key(path));
            when_failed(retval, exit);
        }
    }

    retval = 0;
exit:
    return retval;
}

static int dm_proxy_cleanup(UNUSED amxd_dm_t* dm, UNUSED amxo_parser_t* parser) {
    dm_proxy_remove_subscriptions();

    proxy.dm = NULL;
    proxy.parser = NULL;
    amxc_array_delete(&proxy.proxy_paths, NULL);

    dm_proxy_object_delete();

    return 0;
}

const char* dm_proxy_get_real_path(amxd_path_t* path) {
    size_t size = amxc_array_size(proxy.proxy_paths);
    const char* real_path = NULL;
    const char* proxy_path = NULL;
    const char* param = amxd_path_get_param(path);
    const amxc_var_t* config_po = GET_ARG(&proxy.parser->config, "proxy-object");
    amxc_string_t new_path;

    amxc_string_init(&new_path, 0);

    for(size_t i = size; i > 0; i--) {
        const char* p = (const char*) amxc_array_get_data_at(proxy.proxy_paths, i - 1);
        size_t len = strlen(p);
        if(strncmp(p, amxd_path_get(path, AMXD_OBJECT_TERMINATE), len) == 0) {
            real_path = GET_CHAR(config_po, p);
            if((real_path == NULL) || (*real_path == 0)) {
                continue;
            }
            proxy_path = p;
            if(param != NULL) {
                amxc_string_setf(&new_path, "%s%s", amxd_path_get(path, AMXD_OBJECT_TERMINATE), amxd_path_get_param(path));
            } else {
                amxc_string_setf(&new_path, "%s", amxd_path_get(path, AMXD_OBJECT_TERMINATE));
            }
            amxc_string_replace(&new_path, p, real_path, 1);
            amxd_path_setf(path, false, "%s", amxc_string_get(&new_path, 0));
            break;
        }
    }

    amxc_string_clean(&new_path);
    return proxy_path;
}

const char* dm_proxy_get_translate_path(const char* proxy_obj) {
    const amxc_var_t* config_po = GET_ARG(&proxy.parser->config, "proxy-object");

    return GET_CHAR(config_po, proxy_obj);
}

bool dm_proxy_is_mapped(amxd_object_t* object, const char* rel_path) {
    amxc_string_t path;
    char* obj_path = amxd_object_get_path(object, AMXD_OBJECT_TERMINATE | AMXD_OBJECT_INDEXED);
    const char* real_path = NULL;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "%s%s", obj_path, rel_path == NULL? "":rel_path);
    real_path = dm_proxy_get_translate_path(amxc_string_get(&path, 0));
    free(obj_path);
    amxc_string_clean(&path);

    return (real_path != NULL && real_path[0] != 0);
}

char* dm_proxy_strip_mapped(const char* mapped,
                            const char* real_obj) {

    amxd_path_t real_path;
    amxd_path_t mapped_path;
    uint32_t mapped_depth = 0;
    char* stripped = NULL;

    amxd_path_init(&mapped_path, mapped);
    amxd_path_init(&real_path, real_obj);

    mapped_depth = amxd_path_get_depth(&mapped_path);

    for(uint32_t i = 0; i < mapped_depth; i++) {
        char* path_part = amxd_path_get_first(&real_path, true);
        free(path_part);
    }

    stripped = strdup(amxd_path_get(&real_path, AMXD_OBJECT_TERMINATE));

    amxd_path_clean(&real_path);
    amxd_path_clean(&mapped_path);

    return stripped;
}

int dm_proxy_register(const char* proxy_path, const char* real_path) {
    int retval = 0;
    amxc_var_t* config_po = GET_ARG(&proxy.parser->config, "proxy-object");
    const amxc_htable_t* proxy_objs = NULL;
    amxd_object_t* proxy_obj = NULL;

    when_not_null(amxc_var_get_key(config_po, proxy_path, AMXC_VAR_FLAG_DEFAULT), exit);
    amxc_var_add_key(cstring_t, config_po, proxy_path, real_path);

    proxy_objs = amxc_var_constcast(amxc_htable_t, config_po);
    amxc_array_delete(&proxy.proxy_paths, NULL);
    proxy.proxy_paths = amxc_htable_get_sorted_keys(proxy_objs);

    proxy_obj = amxd_dm_findf(proxy.dm, "%s", proxy_path);
    if(proxy_obj != NULL) {
        amxc_llist_append(&proxy.proxy_base->derived_objects, &proxy_obj->derived_from);
    } else {
        retval = dm_proxy_add_object(proxy.dm, proxy_path);
    }

exit:
    return retval;
}

int dm_proxy_unregister(const char* proxy_path) {
    int retval = -1;
    amxc_var_t* config_po = NULL;
    const amxc_htable_t* proxy_objs = NULL;
    amxc_var_t* info = NULL;
    amxd_object_t* proxy_obj = NULL;

    when_null(proxy.parser, exit);
    config_po = GET_ARG(&proxy.parser->config, "proxy-object");
    info = amxc_var_get_key(config_po, proxy_path, AMXC_VAR_FLAG_DEFAULT);

    retval = 0;
    when_null(info, exit);

    amxc_var_delete(&info);

    proxy_objs = amxc_var_constcast(amxc_htable_t, config_po);
    amxc_array_delete(&proxy.proxy_paths, NULL);
    proxy.proxy_paths = amxc_htable_get_sorted_keys(proxy_objs);

    proxy_obj = amxd_dm_findf(proxy.dm, "%s", proxy_path);
    when_null(proxy_obj, exit);

    dm_proxy_remove_object(proxy.dm, proxy_obj);

exit:
    return retval;
}

amxc_var_t* dm_proxy_get_mappings(void) {
    return GET_ARG(&proxy.parser->config, "proxy-object");
}

amxd_dm_t* dm_proxy_get_dm(void) {
    return proxy.dm;
}

mod_proxy_t* dm_proxy_get_info(void) {
    return &proxy;
}

int _dm_proxy_main(int reason,
                   amxd_dm_t* dm,
                   amxo_parser_t* parser) {
    int retval = 0;
    switch(reason) {
    case 0: // START
        retval = dm_proxy_init(dm, parser);
        break;
    case 1: // STOP
        retval = dm_proxy_cleanup(dm, parser);
        break;
    }

    return retval;
}
