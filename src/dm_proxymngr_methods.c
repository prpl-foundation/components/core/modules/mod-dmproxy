/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "dm_proxy.h"
#include "dm_proxymngr_methods.h"

typedef struct _verify_data {
    uint64_t call_id;
    amxc_var_t table;
    amxc_llist_t requests;
    amxp_timer_t* wait_timer;
    uint32_t refs;
} verify_data_t;

static void verify_done(UNUSED const amxb_bus_ctx_t* bus_ctx,
                        amxb_request_t* request,
                        int status,
                        void* priv) {
    verify_data_t* verify = (verify_data_t*) priv;
    const amxc_htable_t* response = amxc_var_constcast(amxc_htable_t, GETI_ARG(request->result, 0));
    amxc_array_t* objects = amxc_htable_get_sorted_keys(response);
    const char* object = (const char*) amxc_array_get_data_at(objects, 0);
    amxc_var_t* item = NULL;

    amxc_llist_it_take(&request->it);
    verify->refs--;

    item = amxc_var_get_key(&verify->table, object, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, item, status == 0? "OK":"Error");

    if(verify->refs == 0) {
        amxd_function_deferred_call_done(verify->call_id, amxd_status_ok, NULL, &verify->table);
        amxc_var_clean(&verify->table);
        amxc_llist_clean(&verify->requests, NULL);
        amxp_timer_delete(&verify->wait_timer);
        free(verify);
    }

    amxc_array_delete(&objects, NULL);
    amxb_close_request(&request);
}

static void verify_delete_request(amxc_llist_it_t* it) {
    amxb_request_t* request = amxc_container_of(it, amxb_request_t, it);
    amxb_close_request(&request);
}

static void verify_cancel(UNUSED uint64_t call_id, void* priv) {
    verify_data_t* verify = (verify_data_t*) priv;
    amxc_var_clean(&verify->table);
    amxc_llist_clean(&verify->requests, verify_delete_request);
    amxp_timer_delete(&verify->wait_timer);
    free(verify);
}

static void verify_timeout(UNUSED amxp_timer_t* timer, void* priv) {
    verify_data_t* verify = (verify_data_t*) priv;
    amxd_function_deferred_call_done(verify->call_id, amxd_status_ok, NULL, &verify->table);
    amxc_var_clean(&verify->table);
    amxc_llist_clean(&verify->requests, verify_delete_request);
    amxp_timer_delete(&verify->wait_timer);
    free(verify);
}

static void verify_start(const char* object, uint32_t depth, verify_data_t* verify) {
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has(object);
    amxb_request_t* request = NULL;
    amxc_var_t* item = NULL;
    amxc_var_t args;

    amxc_var_init(&args);

    item = amxc_var_add_key(cstring_t, &verify->table, object, "Not found");
    when_null(bus_ctx, exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "depth", depth);

    verify->refs++;
    request = amxb_async_call(bus_ctx, object, "_get", &args, verify_done, verify);
    if(request != NULL) {
        amxc_llist_append(&verify->requests, &request->it);
        amxc_var_set(cstring_t, item, "Not responding"); // is changed to ok when responds
    } else {
        verify->refs--;
        amxc_var_set(cstring_t, item, "Failure");
    }

exit:
    amxc_var_clean(&args);
    return;
}

amxd_status_t _register(UNUSED amxd_object_t* object,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    const char* proxy_obj = GET_CHAR(args, "proxy");
    const char* real_obj = GET_CHAR(args, "real");
    amxd_status_t rv = amxd_status_ok;

    if(dm_proxy_register(proxy_obj, real_obj) != 0) {
        rv = amxd_status_object_not_found;
    }

    return rv;
}

amxd_status_t _unregister(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    const char* proxy_obj = GET_CHAR(args, "proxy");

    dm_proxy_unregister(proxy_obj);

    return amxd_status_ok;
}

amxd_status_t _list(UNUSED amxd_object_t* object,
                    UNUSED amxd_function_t* func,
                    UNUSED amxc_var_t* args,
                    amxc_var_t* ret) {
    amxc_var_t* mappings = dm_proxy_get_mappings();

    amxc_var_copy(ret, mappings);

    return amxd_status_ok;
}

amxd_status_t _verify(UNUSED amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    verify_data_t* data = (verify_data_t*) calloc(1, sizeof(verify_data_t));
    amxd_status_t status = amxd_status_ok;
    uint32_t wait_time = GET_UINT32(args, "wait_time");
    uint32_t depth = GET_UINT32(args, "depth");
    bool verify_mapped = GET_BOOL(args, "verify_mapped");
    amxc_var_t* mappings = GET_ARG(args, "mappings") == NULL ? dm_proxy_get_mappings() : \
        GET_ARG(args, "mappings");

    amxc_var_set_type(&data->table, AMXC_VAR_ID_HTABLE);
    amxc_llist_init(&data->requests);
    data->refs = 1;

    amxc_var_for_each(item, mappings) {
        const char* path = NULL;
        if(verify_mapped) {
            path = amxc_var_key(item);
        } else {
            path = GET_CHAR(item, NULL);
        }
        if((path == NULL) || (*path == 0)) {
            continue;
        }
        amxb_be_cache_remove_path(path);
        verify_start(path, depth, data);
    }

    data->refs--;

    if(data->refs != 0) {
        amxd_function_defer(func, &data->call_id, ret, verify_cancel, data);
        amxp_timer_new(&data->wait_timer, verify_timeout, data);
        amxp_timer_start(data->wait_timer, wait_time * 1000);
        status = amxd_status_deferred;
    } else {
        amxc_var_move(ret, &data->table);
        amxc_var_clean(&data->table);
        amxc_llist_clean(&data->requests, NULL);
        free(data);
    }

    return status;
}
