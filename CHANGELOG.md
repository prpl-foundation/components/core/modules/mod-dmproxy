# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.4.3 - 2024-10-10(10:23:29 +0000)

### Other

- Incorrect responses when some objects are not found

## Release v1.4.2 - 2024-09-25(14:24:20 +0000)

### Other

- [tr181-device] tr181-device crash

## Release v1.4.1 - 2024-05-16(12:27:32 +0000)

### Other

- [tr181-device -D][Memory leak] tr181-device -D is consuming 55MB in 4days

## Release v1.4.0 - 2024-04-18(15:05:04 +0000)

### New

- Extend verify with verify_mapped and mappings

## Release v1.3.9 - 2024-02-07(11:32:03 +0000)

### Other

- [dmproxy] Some parameter paths are not well shown in the dm proxy

## Release v1.3.8 - 2023-12-19(14:28:58 +0000)

### Other

- CLONE - [CHR2fA] tr181-device plugin crash (short term)

## Release v1.3.7 - 2023-11-20(10:11:53 +0000)

### Fixes

- [AMX] Device prefix missing for add responses to search paths

## Release v1.3.6 - 2023-11-17(14:58:35 +0000)

### Fixes

- [LCM][usp] objectcreation and objectdeletion notifications no longer received

## Release v1.3.5 - 2023-09-25(12:36:41 +0000)

### Fixes

- [PRPL][SAFRAN] Wan is not up after reset, only after additional reboot

## Release v1.3.4 - 2023-09-21(10:05:38 +0000)

### Other

- Crash during shutdown

## Release v1.3.3 - 2023-09-15(06:40:07 +0000)

### Other

- [mod_dmproxy] fix missing libsahtrace search path

## Release v1.3.2 - 2023-09-13(09:50:05 +0000)

## Release v1.3.1 - 2023-07-06(11:28:03 +0000)

### Changes

- [DMProxy] Device. datamodel is not responding anymore if a proxied component is no responding

## Release v1.3.0 - 2023-03-23(08:37:01 +0000)

### New

- [pcb] usp endpoint doesn't support pcb requests

## Release v1.2.5 - 2022-11-29(14:48:45 +0000)

### Fixes

- Fix wrong usage of function amxd_path_setf

## Release v1.2.4 - 2022-11-17(07:18:15 +0000)

### Fixes

- forward output arguments to caller

## Release v1.2.3 - 2022-11-09(09:09:16 +0000)

### Fixes

- Delete response is not properly proxied

## Release v1.2.2 - 2022-10-14(09:40:32 +0000)

### Fixes

- [Prpl] [USP] [Get_Msg] Bad Get_resp message when full path of parameter is requested

## Release v1.2.1 - 2022-09-30(08:41:47 +0000)

### Fixes

- [mod_dmproxy] Search expressions do not work with the device prefix

## Release v1.2.0 - 2022-09-26(12:45:05 +0000)

### Changes

- Make it possible to proxy individual instances or template objects

## Release v1.1.2 - 2022-09-14(08:12:24 +0000)

### Fixes

- dm_proxy segfaults when wrong proxy object path is provided

## Release v1.1.1 - 2022-09-01(07:02:51 +0000)

## Release v1.1.0 - 2022-09-01(06:13:46 +0000)

### New

- Issue: Adds debian package generation

## Release v1.0.1 - 2022-08-31(13:12:15 +0000)

### Fixes

- [object mapping]Subcription on root proxy object causes stack overflow

## Release v1.0.0 - 2022-08-26(11:07:13 +0000)

## Release v0.1.4 - 2022-06-09(12:32:12 +0000)

### Other

- mod dmproxy wrong path translation when destination is a parameter in Device.

## Release v0.1.3 - 2022-05-23(08:47:32 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.1.2 - 2022-02-07(15:11:11 +0000)

## Release v0.1.1 - 2021-11-30(11:42:03 +0000)

### Fixes

- Check if subscription exists

## Release v0.1.0 - 2021-11-30(08:50:28 +0000)

### New

- Add event proxy and pipeline definition

